cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME droneSimulatorGridROSModule)
project(${PROJECT_NAME})

### Use version 2011 of C++ (c++11). By default ROS uses c++98
#see: http://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
#see: http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake
#add_definitions(-std=c++11)
#add_definitions(-std=c++0x)
add_definitions(-std=c++03)



# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries





set(DRONE_SIMULATOR_GRID_ROS_MODULE_SOURCE_DIR
    src/sources
)
	
set(DRONE_SIMULATOR_GRID_ROS_MODULE_INCLUDE_DIR
    src/include
)

set(DRONE_SIMULATOR_GRID_ROS_MODULE_HEADER_FILES

    ${DRONE_SIMULATOR_GRID_ROS_MODULE_INCLUDE_DIR}/arenaSimulator.h
    ${DRONE_SIMULATOR_GRID_ROS_MODULE_INCLUDE_DIR}/droneSimulator.h
    ${DRONE_SIMULATOR_GRID_ROS_MODULE_INCLUDE_DIR}/geometry.h

    #Drone Module
    ${DRONE_SIMULATOR_GRID_ROS_MODULE_INCLUDE_DIR}/droneSimulatorGridROSModule.h


)


set(DRONE_SIMULATOR_GRID_ROS_MODULE_SOURCE_FILES

    ${DRONE_SIMULATOR_GRID_ROS_MODULE_SOURCE_DIR}/arenaSimulator.cpp
    ${DRONE_SIMULATOR_GRID_ROS_MODULE_SOURCE_DIR}/droneSimulator.cpp

    #Drone Module
    ${DRONE_SIMULATOR_GRID_ROS_MODULE_SOURCE_DIR}/droneSimulatorGridROSModule.cpp

)
	


#Nodes
set(DRONE_SIMULATOR_GRID_ROS_MODULE_NODE_SOURCE_FILES
    ${DRONE_SIMULATOR_GRID_ROS_MODULE_SOURCE_DIR}/droneSimulatorGridROSModule_Node.cpp
)


find_package(catkin REQUIRED
        COMPONENTS roscpp std_msgs lib_pose lib_pugixml droneModuleROS droneMsgsROS droneSimulator)

find_package(OpenCV REQUIRED)
MESSAGE( STATUS "CMAKE_MODULE_PATH: " ${CMAKE_MODULE_PATH} )


catkin_package(
	DEPENDS OpenCV
        CATKIN_DEPENDS roscpp std_msgs lib_pose lib_pugixml droneModuleROS droneMsgsROS droneSimulator
  )


include_directories(${DRONE_SIMULATOR_GRID_ROS_MODULE_INCLUDE_DIR})
include_directories(${OpenCV_INCLUDE_DIRS})
MESSAGE( STATUS "OpenCV_INCLUDE_DIRS: " ${OpenCV_INCLUDE_DIRS} )
include_directories(${catkin_INCLUDE_DIRS})




add_library(droneSimulatorGridROSModule ${DRONE_SIMULATOR_GRID_ROS_MODULE_NODE_SOURCE_FILES} ${DRONE_SIMULATOR_GRID_ROS_MODULE_SOURCE_FILES} ${DRONE_SIMULATOR_GRID_ROS_MODULE_HEADER_FILES})
add_dependencies(droneSimulatorGridROSModule ${catkin_EXPORTED_TARGETS})
target_link_libraries(droneSimulatorGridROSModule ${OpenCV_LIBS})
target_link_libraries(droneSimulatorGridROSModule ${catkin_LIBRARIES})



#NODE
add_executable(droneSimulatorGrid ${DRONE_SIMULATOR_GRID_ROS_MODULE_NODE_SOURCE_FILES} ${DRONE_SIMULATOR_GRID_ROS_MODULE_SOURCE_FILES} ${DRONE_SIMULATOR_GRID_ROS_MODULE_HEADER_FILES})
add_dependencies(droneSimulatorGrid ${catkin_EXPORTED_TARGETS})
target_link_libraries(droneSimulatorGrid droneSimulatorGridROSModule)
target_link_libraries(droneSimulatorGrid ${OpenCV_LIBS})
target_link_libraries(droneSimulatorGrid ${catkin_LIBRARIES})


