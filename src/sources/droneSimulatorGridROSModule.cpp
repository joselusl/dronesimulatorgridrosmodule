//////////////////////////////////////////////////////
//  droneSimulatorGridROSModule.cpp
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Oct 27, 2013
//      Author: joselusl
//
//////////////////////////////////////////////////////


#include "droneSimulatorGridROSModule.h"



using namespace std;




DroneSimulatorGridROSModule::DroneSimulatorGridROSModule() : Module(droneModule::active,DRONE_SIMULATOR_GRID_RATE)
{

    return;
}


DroneSimulatorGridROSModule::~DroneSimulatorGridROSModule()
{
	close();
	return;
}

int DroneSimulatorGridROSModule::setSensorConfig(std::string sensorConfig)
{
    this->sensorConfig=sensorConfig;
    return 1;
}

int DroneSimulatorGridROSModule::setTopicConfigs(std::string dronePoseSubsTopicName, std::string gridIntersectionsPublTopicName)
{
    this->dronePoseSubsTopicName=dronePoseSubsTopicName;
    this->gridIntersectionsPublTopicName=gridIntersectionsPublTopicName;
    return 1;
}



bool DroneSimulatorGridROSModule::init()
{
    Module::init();


    //Do stuff

    //Set World
    MyDrone.setWorld(&IarcArena);

    //Configure sensor
    MyDrone.setDetectionConfig(sensorConfig);


    //end
    return true;
}


void DroneSimulatorGridROSModule::open(ros::NodeHandle & nIn)
{
	//Node
    Module::open(nIn);


    //Init
    if(!init())
    {
        cout<<"Error init"<<endl;
        return;
    }



    //// TOPICS
    //Subscribers
    //Commands to test
    //rostopic pub -1 /drone0/EstimatedPose_droneGMR_wrt_GFF droneMsgsROS/dronePoseStamped -- '{header: auto, pose:[1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, "a", "b", "c"]}'
    //rostopic pub -1 /drone0/EstimatedPose_droneGMR_wrt_GFF droneMsgsROS/dronePoseStamped -- '{header: auto, pose:[1.0, 5.0, 5.0, 0.0, 0.0, 0.0, 0.0, "a", "b", "c"]}'
    dronePoseSubs = n.subscribe(dronePoseSubsTopicName, 1, &DroneSimulatorGridROSModule::dronePoseCallback, this);



    //Publishers
    gridIntersectionsPubl=n.advertise<droneMsgsROS::points3DStamped>(gridIntersectionsPublTopicName, 1, true);



    //Flag of module opened
    droneModuleOpened=true;

    //autostart
    moduleStarted=true;

	
	//End
	return;
}


void DroneSimulatorGridROSModule::close()
{
    Module::close();



    //Do stuff

    return;
}


bool DroneSimulatorGridROSModule::resetValues()
{
    //Do stuff

    return true;
}


bool DroneSimulatorGridROSModule::startVal()
{
    //Do stuff

    //End
    return Module::startVal();
}


bool DroneSimulatorGridROSModule::stopVal()
{
    //Do stuff

    return Module::stopVal();
}


bool DroneSimulatorGridROSModule::run()
{
    if(!Module::run())
        return false;

    if(droneModuleOpened==false)
        return false;



    //Process simulator of grid
    gridIntersections.clear(); //Clean

    if(!MyDrone.getDetectedGridPoints(gridIntersections))
        return false;



    //Publish
    if(!publishGridIntersections())
    {
        cout<<"error publishing"<<endl;
        return false;
    }

    

    return true;
}

void DroneSimulatorGridROSModule::dronePoseCallback(const droneMsgsROS::dronePoseStamped::ConstPtr& msg)
{
    //Receive
    SL::Pose PoseDrone_wrt_world;
    std::vector<double> tvec(3); //X, Y, Z
    std::vector<double> navAngles(3); //Yaw, Pitch, Roll


    tvec[0]=msg->pose.x;
    tvec[1]=msg->pose.y;
    tvec[2]=msg->pose.z;

    navAngles[0]=msg->pose.yaw;
    navAngles[1]=msg->pose.pitch;
    navAngles[2]=msg->pose.roll;


    PoseDrone_wrt_world.setDroneNavData(tvec,navAngles);

    //Set pose of the drone wrt world
    MyDrone.setPose(PoseDrone_wrt_world);


    return;
}


bool DroneSimulatorGridROSModule::publishGridIntersections()
{
    if(droneModuleOpened==false)
        return false;


    //Transform to message
    droneMsgsROS::points3DStamped gridIntersectionsDetectedMsg;
    //Header
    gridIntersectionsDetectedMsg.header.stamp=ros::Time::now();
    //keypoints
    for (std::vector<cv::Point3f>::iterator it = gridIntersections.begin() ; it != gridIntersections.end(); ++it)
    {
        droneMsgsROS::vector3f point3D;
        point3D.x=it->x;
        point3D.y=it->y;
        point3D.z=it->z;

        gridIntersectionsDetectedMsg.points3D.push_back(point3D);
    }


    //Publish
    gridIntersectionsPubl.publish(gridIntersectionsDetectedMsg);


    return true;

}


